The GitLab repository you're here was solely for demonstration [how this Udemy tutorial](https://www.udemy.com/course/telegram-firebase-bot) works.

## Deploying to Firebase

To deploy your own fork of this repository, you need to clone its repository contents into your machine then deploy it to your own Firebase project.

### Requirements

* An computer with at least Node 10.x (at least LTS version required) and up-to-date npm.
  *
* An Google Account and an Firebase project to be used for deployment
  * We recommend **Blaze Plan** that also extends to additional **Google Cloud Platform** such as BigQuery and other IaaS. See [pricing calucator page]() before you upgrade.

### The Setup

1. Clone this
